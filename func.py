import io
import json
import jwt
import logging
import os
import requests
from fdk import response
from datetime import datetime, timezone


def handler(ctx, data: io.BytesIO = None):

    try:

        tokenIssued = int(datetime.now(tz=timezone.utc).timestamp())
        # These are the parameters you need to substitute for the values for
        tokenExpiry = tokenIssued + 3600
        # your OAC instance and IDCS instance and confidential app
        # The scope also includes 'offline_access' in order to also return a # refresh token
        clientId = os.getenv('CLIENT_ID')
        certAlias = os.getenv('CERT_ALIAS')
        userToAssert = os.getenv('USER_TO_ASSERT')
        OACscope = os.getenv('OAC_SCOPE')
        IDCS_url = os.getenv('IDCS_URL')

        private_key = open(os.getenv('PATH_PRIV_KEY'), 'r').read()

        # The following details are used to create the JWT
        header = {
            "alg": "RS256",
            "typ": "JWT",
            "kid": certAlias
        }

        client_payload = {
            "sub": clientId,
            "iss": clientId,
            "aud": ["https://identity.oraclecloud.com/"],
            "iat": tokenIssued,
            "exp": tokenExpiry
        }

        user_payload = {
            "sub": userToAssert,
            "iss": clientId,
            "aud": ["https://identity.oraclecloud.com/"],
            "iat": tokenIssued,
            "exp": tokenExpiry
        }

        # Create the signed assertions
        encoded_user_assertion = jwt.encode(
            payload=user_payload,
            headers=header,
            key=private_key,
            algorithm="RS256")

        encoded_client_assertion = jwt.encode(
            payload=client_payload,
            headers=header,
            key=private_key,
            algorithm="RS256")

        # Create the payload and headers for the call to the IDCS API to
        # obtain the access token

        payload = {
            'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'scope': OACscope,
            'client_id': clientId,
            'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'assertion': encoded_user_assertion,
            'client_assertion': encoded_client_assertion
        }

        headers = {
            'content-type': 'application/x-www-form-urlencoded'
        }

        # Call the IDCS API and output the access token
        responseToken = requests.request(
            "POST", IDCS_url, headers=headers, data=payload)

        return response.Response(
            ctx, response_data=json.dumps(responseToken.json()),
            headers={"Content-Type": "application/json"})


    except (Exception, ValueError) as ex:
        logging.getLogger().info('error parsing json payload: ' + str(ex))
